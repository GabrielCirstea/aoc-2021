use std::fs;

/* Pop and check if the value is equal with the desired one 
 * @st - the stack to pop from
 * @o - the desired value
 * @return - true if the values are equal, false otherwise */
fn pop_eq(st: &mut Vec<char>, o: char) -> bool {
    if let Some(t) = st.pop() {
        if t != o {
            return false;
        }
    }
    return true;
}
/* If the line is corrupted return the first incorrect character */
fn first_wrong(line: &str) -> Option<char> {
    let mut st: Vec<char> = Vec::new();
    for (i,c) in line.chars().enumerate() {
        match c {
            ')' => {
                if !pop_eq(&mut st, '(') {
                    return Some(')');
                }
            }
            ']' => {
                if !pop_eq(&mut st, '[') {
                    return Some(']');
                }
            }
            '}' => {
                if !pop_eq(&mut st, '{') {
                    return Some('}');
                }
            }
            '>' => {
                if !pop_eq(&mut st, '<') {
                    return Some('>');
                }
            }
            _ => st.push(c),
        }
        if st.is_empty() {
            return None;
        }
    }
    None
}

pub fn part1() {
    let cont = fs::read_to_string("src/day10/input.txt").expect("no file");
    let mut score = 0;
    cont.split('\n').for_each(|l| {
        if let Some(w) = first_wrong(&l) {
            // println!("{}: {}", l, w);
            score += match w {
                ')' => 3,
                ']' => 57,
                '}' => 1197,
                '>' => 25137,
                _ => 0,
            }
        }
    });
    println!("Score: {}", score);
}

/* Compute the stack of the unclosed characters of a given line */
fn find_unclosed(line: &str) -> Option<Vec<char>> {
    let mut st: Vec<char> = Vec::new();
    for c in line.chars() {
        match c {
            ')' => {
                if !pop_eq(&mut st, '(') {
                    return None;
                }
            }
            ']' => {
                if !pop_eq(&mut st, '[') {
                    return None;
                }
            }
            '}' => {
                if !pop_eq(&mut st, '{') {
                    return None;
                }
            }
            '>' => {
                if !pop_eq(&mut st, '<') {
                    return None;
                }
            }
            _ => st.push(c),
        }
    }
    return Some(st);
}

/* Compute the score for the closing sequence of characters for a given line
 * Just add the points for the characters that needs to be added */
fn score_closing(st: &Vec<char>) -> i64 {
    let mut score = 0;
    for c in st.iter().rev() {
        score *= 5;
        score += match c {
            '(' => 1,
            '[' => 2,
            '{' => 3,
            '<' => 4,
            _ => 0,
        }
    }
    return score;
}

pub fn part2() {
    let cont = fs::read_to_string("src/day10/input.txt").expect("no file");
    let mut scores = Vec::new();
    cont.split('\n').for_each(|l| {
        if first_wrong(&l).is_none() {
            if let Some(st) = find_unclosed(&l) {
                let s = score_closing(&st);
                scores.push(s);
                println!("{}: {:?} - {}", l, st, s);
            }
        }
    });
    scores.sort();
    println!("Score: {}", scores[scores.len()/2]);
}
