use std::fs;

pub fn part1() {
    let content = fs::read_to_string("src/day1/input.txt").expect("No file");
    let splits = content.as_str().trim().split('\n');
    let mut inc = 0;
    let mut last :Option<i32> = None;
    for s in splits {
        let c :i32 = s.parse().expect("parse");
        if let Some(n) = last {
            if n < c {
                inc += 1;
            }
            last = Some(c);
        } else {
            last = Some(c);
        }
        println!("{}", c);
    }
    println!("Incs: {}", inc);
}

pub fn part2() {
    let content = fs::read_to_string("src/day1/input.txt").expect("No file");
    let splits = content.as_str().trim().split('\n');
    let values :Vec<i32> = splits.map(|s| s.parse().expect("parse")).collect();
    let mut last_sum :Option<i32> = None;
    let mut inc = 0;
    for i in 0..(values.len()) {
        let sum :i32 = values.iter().skip(i).take(3).sum();
        if let Some(s) = last_sum {
            if sum > s {
                inc += 1;
            }
            last_sum = Some(sum);
        } else {
            last_sum = Some(sum);
        }
    }
    println!("Incs: {}", inc);
}
