use std::fmt;
use std::fs;

fn print_map(m: &[[i32; 1000]; 1000]) {
    for r in m {
        for c in r {
            if *c == 0 {
                print!(".")
            } else {
                print!("{}", c);
            }
        }
        println!("");
    }
}

pub fn part1() {
    let mut map = [[0; 1000]; 1000];
    let cont = fs::read_to_string("src/day5/sample.txt").expect("no file");
    let lines = cont.as_str().trim().split('\n');
    for l in lines {
        let (start, end) = l.split_once("->").unwrap();
        let (xs, ys) = start.split_once(',').unwrap();
        let mut xs: i32 = xs.trim().parse().unwrap();
        let mut ys: i32 = ys.trim().parse().unwrap();

        let (xe, ye) = end.split_once(',').unwrap();
        let xe: i32 = xe.trim().parse().unwrap();
        let ye: i32 = ye.trim().parse().unwrap();

        let dir_x = match xs - xe {
            x if x > 0 => -1,
            x if x < 0 => 1,
            0 => 0,
            _ => 0,
        };
        let dir_y = match ys - ye {
            x if x > 0 => -1,
            x if x < 0 => 1,
            _ => 0,
        };

        // limit to only vertical and horizontal lines
        if dir_x != 0 && dir_y != 0 {
            continue;
        }

        while xs != xe || ys != ye {
            map[ys as usize][xs as usize] += 1;
            xs += dir_x;
            ys += dir_y;
        }
        map[ys as usize][xs as usize] += 1;
    }
    let min_2: i32 = map
        .iter()
        .map(|l| l.iter().filter(|&c| *c > 1).count() as i32)
        .sum::<i32>();
    println!("sum: {}", min_2);
}

pub fn part2() {
    let mut map = [[0; 1000]; 1000];
    let cont = fs::read_to_string("src/day5/sample.txt").expect("no file");
    let lines = cont.as_str().trim().split('\n');
    for l in lines {
        let (start, end) = l.split_once("->").unwrap();
        let (xs, ys) = start.split_once(',').unwrap();
        let mut xs: i32 = xs.trim().parse().unwrap();
        let mut ys: i32 = ys.trim().parse().unwrap();

        let (xe, ye) = end.split_once(',').unwrap();
        let xe: i32 = xe.trim().parse().unwrap();
        let ye: i32 = ye.trim().parse().unwrap();

        let dir_x = match xs - xe {
            x if x > 0 => -1,
            x if x < 0 => 1,
            0 => 0,
            _ => 0,
        };
        let dir_y = match ys - ye {
            x if x > 0 => -1,
            x if x < 0 => 1,
            _ => 0,
        };

        while xs != xe || ys != ye {
            map[ys as usize][xs as usize] += 1;
            xs += dir_x;
            ys += dir_y;
        }
        map[ys as usize][xs as usize] += 1;
    }
    let min_2: i32 = map
        .iter()
        .map(|l| l.iter().filter(|&c| *c > 1).count() as i32)
        .sum::<i32>();
    println!("sum: {}", min_2);
}
