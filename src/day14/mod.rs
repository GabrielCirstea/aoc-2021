use std::collections::HashMap;
use std::fs;
use std::iter::FromIterator;

// apply th polymorfism stuff to a string
// returns a new string
fn polymerization(template: &String, map_p: &HashMap<String, String>) -> String {
    let mut result = String::new();
    result.push(template.chars().take(1).nth(0).unwrap());

    for i in 1..template.len() {
        result.push_str(
            map_p
                .get(&template.chars().skip(i - 1).take(2).collect::<String>())
                .unwrap(),
        );
        result.push(template.as_bytes()[i] as char);
    }

    return result;
}

fn most_last(polyf: &str) -> i32 {
    let mut freq = [0; 30];

    for c in polyf.chars() {
        let pos = c as u32 - 'A' as u32;
        freq[pos as usize] += 1;
    }

    let mx = freq.iter().max().unwrap();
    let mn = freq.iter().filter(|&c| *c != 0).min().unwrap();

    // get min

    return mx - mn;
}

pub fn part1() {
    let content = fs::read_to_string("src/day14/input.txt").expect("No file");
    let (template, pairs) = content.as_str().trim().split_once("\n\n").unwrap();

    let mut map_p = HashMap::new();
    map_p.insert("key1".to_string(), "val1".to_string());

    println!("templat: {}", template);
    // fancy and ugly looking parsing and storing in a hash map
    let map_p: HashMap<String, String> = HashMap::from_iter(
        pairs
            .lines()
            .map(|l| {
                let (f, s) = l.split_once("->").unwrap();
                (f.trim().to_string(), s.trim().to_string())
            })
            .collect::<Vec<(String, String)>>(),
    );

    for (s, d) in &map_p {
        println!("{s} -> {d}");
    }

    let mut to_poly = template.to_string().clone();
    for i in 0..10 {
        let result = polymerization(&to_poly, &map_p);
        println!("Step {}: {}", i, result);
        to_poly = result;
    }

    println!("{}", most_last(&to_poly));
}

fn map_append(map: &mut HashMap<String, i64>, k: &str, v: i64) {
    let val = map.entry(k.to_string()).or_insert(0);
    *val += v;
}

// get the pairs from a string
fn get_pairs(template: &str, map_f: &mut HashMap<String, i64>) {
    let sl:&[u8] = template.as_bytes();
    for w in sl.windows(2) {
        let chars = std::str::from_utf8(w).unwrap();
        map_append(map_f, chars, 1);
    }
}

// for each pair, generate the resulting pairs, the one before and after the new
// inserted letter. Also keep track of the new added letters
fn smart_step(
    map_f: &mut HashMap<String, i64>,
    map_p: &HashMap<String, String>,
    freq: &mut [i64; 30],
) {
    let mut map_c: HashMap<String, i64> = HashMap::new();
    for (k, v) in map_f.into_iter() {
        if *v == 0 {
            continue;
        }
        let to_go = map_p.get(k).unwrap();
        let k :Vec<char> = k.chars().collect();
        let mut dest1: String = String::from(k[0]);
        dest1.push_str(to_go);
        let mut dest2: String = to_go.clone();
        dest2.push_str(&String::from(k[1]));
        map_append(&mut map_c, &dest1, *v);
        map_append(&mut map_c, &dest2, *v);

        for c in to_go.chars() {
            let pos = c as i32 - 'A' as i32;
            freq[pos as usize] += *v as i64;
        }

        // eliminate the previous pair
        *v = 0;
    }
    for (k, v) in map_c {
        map_f.insert(k.to_string(), v);
    }
}

fn count_chars(freq: [i64;30]) -> i64 {
    let mx = freq.iter().max().unwrap();
    let mn = freq.iter().filter(|&c| *c != 0).min().unwrap();

    println!("\n{} - {}", mx, mn);

    return mx - mn;
}

pub fn part2() {
    let content = fs::read_to_string("src/day14/sample.txt").expect("No file");
    let (template, pairs) = content.as_str().trim().split_once("\n\n").unwrap();

    println!("templat: {}", template);
    // fancy and ugly looking parsing and storing in a hash map
    let map_p: HashMap<String, String> = HashMap::from_iter(
        pairs
            .lines()
            .map(|l| {
                let (f, s) = l.split_once("->").unwrap();
                (f.trim().to_string(), s.trim().to_string())
            })
            .collect::<Vec<(String, String)>>(),
    );

    let mut map_f: HashMap<String, i64> = HashMap::new();
    for k in map_p.keys() {
        map_f.insert(k.to_string(), 0);
    }

    // count the current characters
    let mut freq = [0; 30];
    for c in template.chars() {
        let pos = c as i32 - 'A' as i32;
        freq[pos as usize] += 1;
    }

    // get a map with the current pairs in the string
    get_pairs(&template, &mut map_f);
    for i in 0..40 {
        smart_step(&mut map_f, &map_p, &mut freq);
    }
    for (s, d) in &map_f {
        println!("{s} = {d}");
    }

    println!("{}", count_chars(freq));
}
