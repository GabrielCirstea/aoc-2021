use std::fs;

pub fn part1() {
    let content = fs::read_to_string("src/day2/input.txt").expect("No file");
    let splits = content.as_str().trim().split('\n');
    let mut depth :i32 = 0;
    let mut horiz = 0;
    splits.for_each(|s| {
        let (dir, val) = s.split_once(' ').expect("No split");
        let val :i32 = val.parse().expect("no int");
        match dir {
            "up" => depth -= val,
            "down" => depth += val,
            "forward" => horiz += val,
            _ => (),
        }
    });
    println!("depth: {}, horizontal: {}", depth, horiz);
    println!("Answer: {}", depth * horiz);
}

pub fn part2() {
    let content = fs::read_to_string("src/day2/input.txt").expect("No file");
    let splits = content.as_str().trim().split('\n');
    let mut depth :i32 = 0;
    let mut horiz = 0;
    let mut aim = 0;
    splits.for_each(|s| {
        let (dir, val) = s.split_once(' ').expect("No split");
        let val :i32 = val.parse().expect("no int");
        match dir {
            "up" => aim -= val,
            "down" => aim += val,
            "forward" => {
                horiz += val;
                depth += aim * val;
            },
            _ => (),
        }
    });
    println!("depth: {}, horizontal: {}", depth, horiz);
    println!("Answer: {}", depth * horiz);

}
