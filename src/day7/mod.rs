use std::fmt;
use std::fs;

/* Calculate the cost to align the crabs to this position
 * @pos - postition to align to
 * @v - positions of crabs
 * @return - the cost */
fn align_to(pos :i32, v :&Vec<i32>) -> i32 {
    let mut cost = 0;
    for c in v {
        cost += (pos - c).abs();
    }
    return cost;
}

pub fn part1() {
    let cont = fs::read_to_string("src/day7/input.txt").expect("no file");
    let nums :Vec<i32> = cont.trim().split(',').map(|n| n.parse().unwrap()).collect();
    let max_pos = nums.iter().max().unwrap();
    let mut min = i32::MAX;
    for i in 0..*max_pos {
        let cost = align_to(i, &nums);
        println!("pso: {} - cost: {}", i, cost);
        if cost < min {
            min = cost;
        }
    }
    println!("Minimum cost: {}", min);
}

/* Same as align_to(), but use arithmethic progression */
fn align2_to(pos :i32, v :&Vec<i32>) -> i32 {
    let mut cost = 0;
    for c in v {
        let dist = (pos-c).abs();
        cost += (dist * (dist+1))/2
    }
    return cost;
}

pub fn part2() {
    let cont = fs::read_to_string("src/day7/input.txt").expect("no file");
    let nums :Vec<i32> = cont.trim().split(',').map(|n| n.parse().unwrap()).collect();
    let max_pos = nums.iter().max().unwrap();
    let mut min = i32::MAX;
    for i in 0..*max_pos {
        let cost = align2_to(i, &nums);
        // println!("pso: {} - cost: {}", i, cost);
        if cost < min {
            min = cost;
        }
    }
    println!("Minimum cost: {}", min);
}
