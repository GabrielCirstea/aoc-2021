use std::fmt;
use std::fs;

fn day_pass(v :&mut Vec<i32>) {
    let mut new_v = Vec::new();
    v.iter_mut().for_each(|f| match f {
        0 => {
            *f = 6;
            new_v.push(8);
        }
        _ => *f -= 1,
    });
    v.append(&mut new_v);
}

pub fn part1() {
    let cont = fs::read_to_string("src/day6/sample.txt").expect("no file");
    let mut nums :Vec<i32> = cont.trim().split(',').map(|n| n.parse().unwrap()).collect();
    for i in 0..80 {
        day_pass(&mut nums);
        // println!("After {} days: {:?}", i+1, nums);
    }
    println!("Total: {}", nums.len());
}

/* fish: (nr_days, pairity)
 * parity: 0..6 -> on what day the fish will spawn another
 * use a freq vec, index is parity */
fn day_smart(v :&mut [i64; 9]) {
    let reset = v[0];
    v[0] = 0;
    for i in 0..8 {
        v[i] = v[i+1]
    }
    v[6] += reset;
    v[8] = reset;
}

pub fn part2() {
    let cont = fs::read_to_string("src/day6/input.txt").expect("no file");
    let nums :Vec<i32> = cont.trim().split(',').map(|n| n.parse().unwrap()).collect();
    let mut fish = [0; 9];
    println!("{:?}", nums);
    for n in nums {
        fish[n as usize] += 1;
    }
    for i in 0..256 {
        day_smart(&mut fish);
        println!("day: {}", i+1);
        for i in 0..9 {
            println!("fish[{}]={}", i, fish[i]);
        }
    }
    println!("Total: {}", fish.iter().sum::<i64>());
}
