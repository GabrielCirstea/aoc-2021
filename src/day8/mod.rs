use std::fs;

pub fn part1() {
    let cont = fs::read_to_string("src/day8/input.txt").expect("no file");
    let lines = cont.trim().split('\n');
    let mut total = 0;
    for l in lines {
        let (_, da) = l.split_once('|').unwrap();
        let nr = da
            .trim()
            .split_whitespace()
            .filter(|w| match w.len() {
                2 | 3 | 4 | 7 => true,
                _ => false,
            })
            .count();
        println!("{} - {}", da, nr);
        total += nr;
    }
    println!("Total: {}", total);
}

/* get the first char in a that is not in b */
fn uncommon_char(a: &str, b: &str) -> Option<char> {
    for c in a.chars() {
        if b.find(c).is_none() {
            return Some(c);
        }
    }
    return None;
}

/* Returns the number of common chars */
fn common_chars(a: &str, b: &str) -> i32 {
    let mut nr = 0;
    for c in a.chars() {
        if b.find(c).is_some() {
            nr += 1;
        }
    }
    return nr;
}

/*
 *  aaaa 
 * b    c
 * b    c
 *  dddd 
 * e    f
 * e    f
 *  gggg 
*/
/* 0 - 6 chras, does not contain 4, contains 1
 * 1 - 2 chars, unique
 * 2 - 5 chars, 2 coomon chars with 4
 * 3 - 5 chars, contains 7
 * 4 - 4 chars, unique
 * 5 - 5 chars, 3 commons chars with 4
 * 6 - 6 chars, does not contain 1
 * 7 - 3 chars, unique
 * 8 - 7 chras, unique
 * 9 - 6 chars, contains 4 and 7 */
fn translate_code(code: &str, signals: &str) -> i32 {
    match code.len() {
        2 => return 1,  // 1
        3 => return 7,  // 7
        4 => return 4,  // 4
        5 => {
            // 2
            let four = signals.split_whitespace().find(|s| s.len() == 4).unwrap();
            if common_chars(code, four) == 2 {
                return 2;
            }
            // 3
            let seven = signals.split_whitespace().find(|s| s.len() == 3).unwrap();
            if uncommon_char(seven, code).is_none() {
                return 3;
            }
            // 5
            if common_chars(code, four) == 3 {
                return 5;
            }
        },
        6 => {
            // 0
            let one = signals.split_whitespace().find(|s| s.len() == 2).unwrap();
            let four = signals.split_whitespace().find(|s| s.len() == 4).unwrap();
            if uncommon_char(four, code).is_some() && uncommon_char(one, code).is_none() {
                return 0;
            }
            // 6
            if uncommon_char(one, code).is_some() {
                return 6;
            }
            return 9;
        },
        7 => return 8,
        _ => (),
    }
    0
}

pub fn part2() {
    let cont = fs::read_to_string("src/day8/input.txt").expect("no file");
    let lines = cont.trim().split('\n');
    let mut total = 0;
    for l in lines {
        let (signals, nums) = l.split_once('|').unwrap();
        let mut nr = 0;
        for n in nums.split_whitespace() {
            nr *= 10;
            nr += translate_code(n, &signals);
        }
        println!("{}: {}",nums, nr);
        total += nr;
    }
    println!("Total: {}", total);
}
