use std::fs;
use std::str::FromStr;

#[derive(Debug)]
struct Point {
    x: i32,
    y: i32
}

#[derive(Debug)]
enum Fold {
    X(i32),
    Y(i32)
}

impl FromStr for Point {
    type Err = std::string::ParseError;

    fn from_str(s :&str) -> Result<Self, Self::Err> {
        let (x, y) = s.split_once(",").unwrap();

        return Ok(Point{
            x: x.parse().unwrap(),
            y: y.parse().unwrap(),
        })
    }
}

impl FromStr for Fold {
    type Err = std::string::ParseError;

    fn from_str(s :&str) -> Result<Self, Self::Err> {
        let (ax, val) = s.split_once("fold along ").unwrap().1.split_once("=").unwrap();

        let res = match ax {
            "x" => Fold::X(val.parse().unwrap()),
            "y" => Fold::Y(val.parse().unwrap()),
            _ => unreachable!(),
        };
        return Ok(res);
    }
}
const SIZE_W :usize = 1350;
const SIZE_H :usize = 1000;
 struct Paper {
     m: [[bool; SIZE_W]; SIZE_H],
     width: usize,
     height: usize

 }

impl Paper {
    fn new() -> Self {
        return Self {
            m: [[false; SIZE_W]; SIZE_H],
            width: SIZE_W,
            height: SIZE_H,
        }
    }

    fn display(&self) {
        for i in 0..self.height {
            let mut row = String::new();
            for j in 0..self.width {
                if self.m[i][j] {
                    row.push_str("#");
                } else {
                    row.push_str(".");
                }
            }
            println!("{}", row);
        }
    }

    fn fold(&mut self, dir: Fold) {
        match dir {
            Fold::X(x) => self.fold_x(x as usize),
            Fold::Y(y) => self.fold_y(y as usize),
            _ => unreachable!()
        }
    }

    // horizontall line to fold on
    // --------
    fn fold_y(&mut self, y: usize) {
        for i in 0..(y+1) {
            for j in 0..self.width {
                self.m[y-i][j] |= self.m[y+i][j];
            }
        }
        self.height = y;
    }

    // vertical line
    // |
    // |
    // |
    fn fold_x(&mut self, x: usize) {
        for i in 0..self.height {
            for j in 0..(x+1) {
                self.m[i][x-j] |= self.m[i][x+j];
            }
        }

        self.width = x;
    }

    fn count(&self) -> i32 {
        let mut dots = 0;
        for i in 0..self.height {
            for j in 0..self.width {
                if self.m[i][j] {
                    dots += 1;
                }
            }
        }
        return dots;
    }
}

pub fn part1() {
    let content = fs::read_to_string("src/day13/input.txt").expect("No file");
    let (points, folds) = content.as_str().trim().split_once("\n\n").unwrap();

    let points: Vec<Point> = points.lines().map(str::parse).map(Result::unwrap).collect();
    let folds: Vec<Fold> = folds.lines().map(str::parse).map(Result::unwrap).collect();

    let mut pap = Paper::new();

    for p in &points {
        println!("{:?}", p);
        pap.m[p.y as usize][p.x as usize] = true;
    }
    println!("len: {}", points.len());

    for f in folds {
        println!("{:?}", f);
        pap.fold(f);
        // pap.display();
        println!("Dots: {}", pap.count());

        // uncoment for part 1
        // break
    }

    pap.display();

}
