use std::fmt;
use std::fs;

struct Cell {
    val: i32,
    mark: bool,
}

impl fmt::Debug for Cell {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}{}", self.val, if self.mark { "<" } else { "" })
    }
}

impl Cell {
    fn new(val: i32) -> Self {
        Cell {
            val: val,
            mark: false,
        }
    }
}

// a matrix wanna be
type Matrix<T> = Vec<Vec<T>>;

fn print_matrix(m: &Matrix<Cell>) {
    for l in m {
        l.iter().for_each(|n| print!("{:?} ", n));
        println!("");
    }
}

/* Read a matrix from a list of lines
 * @cont - the content
 * @return - a Matrix */
fn read_matrix(cont: &str) -> Option<Matrix<Cell>> {
    let mut m: Matrix<Cell> = Matrix::with_capacity(5);
    for r in cont.split('\n') {
        let line: Vec<Cell> = r
            .split_ascii_whitespace()
            .map(|n| Cell::new(n.parse().unwrap()))
            .collect();
        m.push(line);
    }
    return Some(m);
}

/* Check if there are all numbers mark on one of the lines */
fn matrix_check_lines(m: &Matrix<Cell>) -> bool {
    for l in m {
        if l.iter().all(|c| c.mark == true) {
            return true;
        }
    }
    false
}

/* Check if there is a column with all numbers marked */
fn matrix_check_columns(m: &Matrix<Cell>) -> bool {
    for c in 0..5 {
        let mut ok = true;
        for l in 0..5 {
            if m[l][c].mark == false {
                ok = false;
            }
        }
        if ok {
            return true;
        }
    }
    return false;
}

/* Find the number n, on matrix and checked it
 * @m - matrix to search into
 * @n - number that we are searching for */
fn matrix_find_and_mark(m: &mut Matrix<Cell>, n: i32) {
    for mut l in m {
        if let Some(i) = l.iter().position(|e| e.val == n) {
            l[i].mark = true;
        }
    }
}

/* Compute the sum of the unmarked numbers */
fn matrix_sum_unmarked(m: &Matrix<Cell>) -> i32 {
    m.iter()
        .map(|r| r.iter().filter(|c| !c.mark).map(|c| c.val).sum::<i32>())
        .sum()
}

/* For each number, find it and check it on every board after this check if
 * there is any matrix that has a line or colum checked */
fn bad_solution(boards: &mut Vec<Matrix<Cell>>, nums: &Vec<i32>) {
    for n in nums {
        for b in &mut *boards {
            matrix_find_and_mark(b, *n);
            if matrix_check_lines(&b) || matrix_check_columns(&b) {
                println!("Winner:");
                print_matrix(&b);
                let sum_unmarked = matrix_sum_unmarked(&b);
                println!("Sum unmarked: {}", sum_unmarked);
                println!("Last number: {}", n);
                println!("Score: {}", sum_unmarked * n);
                return;
            }
        }
    }
}

/* Read and parse the input */
fn get_input(file: &str) -> (Vec<i32>, Vec<Matrix<Cell>>) {
    let cont = fs::read_to_string(file).expect("no file");
    let nums: Vec<&str> = cont.as_str().trim().split('\n').take(1).collect();
    let nums: Vec<&str> = nums[0].split(',').collect();
    let nums: Vec<i32> = nums.iter().map(|n| n.parse().unwrap()).collect();
    println!("nums: {:?}", nums);

    let rows: Vec<&str> = cont.as_str().trim().split("\n\n").skip(1).collect();
    // rows.for_each(|r| println!("{}", r));
    let mut boards: Vec<Matrix<Cell>> = rows.iter().map(|b| read_matrix(b).unwrap()).collect();

    return (nums, boards);
}

pub fn part1() {
    let (nums, mut boards) = get_input("src/day4/sample.txt");
    for b in &boards {
        print_matrix(&b);
        println!("");
    }
    bad_solution(&mut boards, &nums);
}

fn bad2_solution(boards: &mut Vec<Matrix<Cell>>, nums: &Vec<i32>) {
    let mut wins: Vec<bool> = vec![false; boards.len() - 1];
    let mut last = 0;
    let mut last_no = 0;
    for n in nums {
        for i in 0..(boards.len() - 1) {
            matrix_find_and_mark(&mut boards[i], *n);
            if matrix_check_lines(&boards[i]) || matrix_check_columns(&boards[i]) {
                if !wins[i] {
                    wins[i] = true;
                    last = i;
                    last_no = *n;
                }
                if wins.iter().all(|&w| w == true) {
                    break;
                }
            }
        }
    }
    println!("Last: {}", last_no);
    print_matrix(&boards[last]);
    let sum_unmarked = matrix_sum_unmarked(&boards[last]);
    println!("Sum unmarked: {}", sum_unmarked);
    println!("Last number: {}", last_no);
    println!("Score: {}", sum_unmarked * last_no);
}

pub fn part2() {
    let (nums, mut boards) = get_input("src/day4/sample.txt");
    bad2_solution(&mut boards, &nums);
}
