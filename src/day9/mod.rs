use std::fs;

/* Read file and build a 2d array of i32 */
fn get_input(file: &str) -> Vec<Vec<i32>> {
    let cont = fs::read_to_string(file).expect("no file");
    let lines = cont.trim().split('\n');
    let mut m: Vec<Vec<i32>> = Vec::new();
    for l in lines {
        let v: Vec<i32> = l.as_bytes().iter().map(|u| (*u -b'0') as i32).collect();
        m.push(v);
    }
    return m;
}

fn print_matrix(m: &Vec<Vec<i32>>) {
    for l in m {
        l.iter().for_each(|n| print!("{}", n));
        println!("");
    }
}

/* Get the value from the matrix at coordinates x and y if are valid */
fn get_v(m: &Vec<Vec<i32>>,x: i32, y: i32) -> Option<i32> {
    if x < 0 || x >= m.len() as i32 {
        return None;
    }
    if y < 0 || y >= m[0].len() as i32 {
        return None;
    }
    let x = x as usize;
    let y = y as usize;
    return Some(m[x][y]);
}

/* Check if m[x][y] is a low point */
fn is_low_point(m: &Vec<Vec<i32>>, x: i32, y: i32) -> bool {
    let l = x as usize;
    let c = y as usize;
    if m[l][c] >= get_v(&m, x-1, y).unwrap_or(i32::MAX) {
        return false;
    }
    if m[l][c] >= get_v(&m, x+1, y).unwrap_or(i32::MAX) {
        return false;
    }
    if m[l][c] >= get_v(&m, x, y-1).unwrap_or(i32::MAX) {
        return false;
    }
    if m[l][c] >= get_v(&m, x, y+1).unwrap_or(i32::MAX) {
        return false;
    }
    return true;
}

pub fn part1() {
    let m = get_input("src/day9/input.txt");
    print_matrix(&m);
    let mut risk = 0;
    for (i, l) in m.iter().enumerate() {
        for (j, n)  in l.iter().enumerate() {
            if is_low_point(&m, i as i32, j as i32) {
                println!("{}: {} {}", n, i, j);
                risk += n+1;
            }
        }
    }
    println!("Risk = {}", risk);
}

/* Do a BF on the matrix, and return the size of the basin
 * Just go up, down, legt, write until a value of 9 */
fn bf(m :&mut Vec<Vec<i32>>, x: usize, y: usize) -> i32 {
    // queue of the nodes to vizit; 
    if m[x][y] >= 9 {
        return 0;
    }
    let mut q :Vec<(i32, i32)>= vec![(x as i32,y as i32)];
    let mut size = 0;
    while !q.is_empty() {
        let (x,y) = q.pop().unwrap();
        if m[x as usize][y as usize] < 9 {
            size += 1;
        }
        for (i, j) in [(-1,0), (1, 0), (0, -1), (0, 1)] {
            if let Some(v) = get_v(&m, x+i, y+j) {
                if v < 9 {
                    q.push((x+i,y+j));
                }
            }
        }
        m[x as usize][y as usize] = 9;
    }
    return size;
}

/* Get the index of the minimum value in a slice(array) of i32 */
fn index_min(v :&[i32]) -> usize {
    let mut i = 0;
    for n in 1..v.len() {
        if v[n] < v[i] {
            i = n;
        }
    }
    return i;
}

pub fn part2() {
    let mut m = get_input("src/day9/input.txt");
    // print_matrix(&m);
    let mut sizes = [0, 0, 0];
    for i in 0..m.len()-1 {
        for j in 0..m[0].len()-1 {
            let res = bf(&mut m, i, j);
            let pos = index_min(&sizes);
            if res > sizes[pos] {
                sizes[pos] = res;
            }
        }
    }
    println!("{}", sizes.iter().product::<i32>());
}
