use std::fs;

pub fn part1() {
    let cont = fs::read_to_string("src/day3/input.txt").expect("no file");
    let nums :Vec<&str> = cont.as_str().trim().split('\n').collect();
    let mut g :String = String::new();
    let mut e :String = String::new();
    for i in 0..nums[0].len() {
        let mut one = 0;
        let mut zero = 0;
        for n in &nums {
            match n.as_bytes()[i] {
                b'0' => zero += 1,
                b'1' => one += 1,
                _ => (),
            }
        }
        if one > zero {
            g.push('1');
            e.push('0');
        } else {
            e.push('1');
            g.push('0');
        }
    }
    println!("g: {}, e: {}", g, e);
    let g = i32::from_str_radix(g.as_str(), 2).unwrap();
    let e = i32::from_str_radix(e.as_str(), 2).unwrap();
    println!("Ans: {}", g*e);
}

// find which bite is most common
// @nums - vector with str of bites to search for the common bite
// @pos - to position we are counting
// @returns - the most common bite
fn most_common(nums :&Vec<&str>, pos :usize) -> usize {
    let nums_len = nums.len();
    let ones = nums.iter().filter(|n| n.as_bytes()[pos] == b'1').count();
    return ones;
}

// oxygen criteria
fn o2_crit(ones :usize, total :usize) -> char {
    return if ones >= total - ones { '1' } else { '0' };
}

// co2 criteria
fn co2_crit(ones :usize, total :usize) -> char {
    return if ones < total - ones { '1' } else { '0' };
}

// get the value depending of the criteria (oxygen or co2)
// @nums - vector with the str fot number in binary
// @crit - function for the criteria
// @returns - the value of oxygen or co2
fn get_value(nums :&Vec<&str>, crit :&Fn(usize, usize) -> char) -> i32 {
    let mut indx :Vec<usize> = vec![0; nums.len()];
    for i in 0..indx.len() {
        indx[i] = i;
    }

    for i in 0..nums[0].len() {
        let mut refs :Vec<&str> = Vec::with_capacity(indx.len());
        for i in 0..refs.capacity() {
            refs.push(nums[indx[i]]);
        }
        let ones = most_common(&refs, i);
        let bite :char = crit(ones, refs.len());
        let mut rem_indx :Vec<usize> = Vec::new();
        for (j, k) in indx.iter().enumerate() {
            if nums[*k].as_bytes()[i] != bite as u8 {
                rem_indx.push(j);
            }
        }
        for j in rem_indx.iter().rev() {
            indx.remove(*j);
        }
        // println!("indx: {:?}", indx);
        for i in &indx {
            // println!("nums[{}]={}", i, nums[*i]);
        }
        if indx.len() == 1 {
            return i32::from_str_radix(nums[indx[0]], 2).unwrap();
        }
    }
    1
}

pub fn part2() {
    let cont = fs::read_to_string("src/day3/input.txt").expect("no file");
    let nums :Vec<&str> = cont.as_str().trim().split('\n').collect();
    // println!("test: {}", most_common(&nums, 2));
    let oxygen = get_value(&nums, &o2_crit);
    let co2 = get_value(&nums, &co2_crit);
    println!("O2: {}", oxygen);
    println!("CO2: {}", co2);
    println!("Ans: {}", oxygen*co2);
}
