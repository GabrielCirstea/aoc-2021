use std::fs;

struct Matrix {
    // tr :Vec<String>,
    // map :Vec<Vec<i32>>
    tr :Vec<String>,
    map :[[i32; 30]; 30]
}

impl Matrix {
    fn add_cave(&mut self, cave :&str) -> Option<usize> {
        if !self.tr.contains(&cave.to_string()) {
            self.tr.push(cave.to_string());
        }
        return self.tr.iter().position(|c| c == cave);
    }

    fn index_of(&self, s: &str) -> usize {
        return self.tr.iter().position(|c| c == s).unwrap();
    }

    fn parse_line(&mut self, line: &str) -> bool {
        let (s, e) = line.trim().split_once('-').expect("parse_line");
        let cs = self.add_cave(s).unwrap();
        let ce = self.add_cave(e).unwrap();
        self.map[cs][ce] = 1;
        self.map[ce][cs] = 1;
        true
    }

    fn print_map(&self) {
        for (n, line) in self.map.iter().enumerate() {
            let mut str_l = String::new();
            if n < self.tr.len() {
                str_l.push_str(format!("{}:\t", self.tr[n]).as_str());
            }
            for (i, c) in line.iter().enumerate() {
                if *c != -1 {
                    str_l.push_str(format!("{}", self.tr[i]).as_str());
                }
                else {
                    str_l.push_str("_");
                }
                str_l.push_str(" ");
            }
            println!("{}", str_l);
        }
    }

    fn print_path(&self, path :&Vec<i32>) {
        let mut str_p = String::new();
        for p in path {
            str_p.push_str(format!("{}, ", self.tr[*p as usize]).as_str());
        }
        println!("{}", str_p);
    }

    /* Tell if we can visit a node */
    fn visit_condition(&self, node :usize, path :&Vec<i32>) -> bool {
        if self.tr[node] == "start" {
            return false;
        }
        // cehck duplicates - for part2
        let is_lower = |c :usize| self.tr[c].chars().all(|l| l.is_lowercase());
        let mut limit = 2;
        for c in path {
            if is_lower(*c as usize) {
                let n = path.iter().filter(|&x| x == c).count();
                if n > 1 {
                    limit = 1;
                }
            }
        }
        // if small cave
        if is_lower(node ) {
            let n = path.iter().filter(|&c| *c == node as i32).count();
            if n >= limit {
                return false
            }
        }
        return true;
    }
    /* Do a bf walk
     * return the numbe of paths to the End */
    fn bf(&self, node :usize, path :&mut Vec<i32>) -> i32 {
        path.push(node as i32);
        let init_len = path.len();
        if self.tr[node] == "end" {
            // self.print_path(&path);
            return 1;
        }
        let mut paths = 0;
        for (i, _) in self.map[node].iter().enumerate().filter(|(_, &c)| c != -1) {
            if self.visit_condition(i, &path) {
                // println!("From {} to {}", node, i);
                paths += self.bf(i as usize, path);
                path.truncate(init_len);
            } 
        }

        return paths;
    }
}

pub fn part1() {
    let content = fs::read_to_string("src/day12/input.txt").expect("No file");
    let splits = content.as_str().trim().split('\n');
    let mut caves = Matrix{
        tr: Vec::new(),
        map: [[-1; 30]; 30]
    };
    for s in splits {
        println!("{}", s);
        caves.parse_line(&s);
    }
    println!("");
    caves.print_map();
    println!("Start: {}\nEnd: {}", caves.index_of("start"),
        caves.index_of("end"));
    println!("Paths: {}", caves.bf(caves.index_of("start"), &mut vec![]));
}
