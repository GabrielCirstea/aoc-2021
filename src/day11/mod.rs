use std::fs;

struct Octo(i32, bool);
/* For the damn map*/
struct Matrix {
    map: Vec<Vec<Octo>>,
}

impl Matrix {
    fn new() -> Self {
        let m = Matrix { map: Vec::new() };
        return m;
    }

    fn add_line(&mut self, line: &str) {
        let l: Vec<i32> = line.as_bytes().iter().map(|&c| (c - b'0') as i32).collect();
        let octos :Vec<Octo> = l.iter().map(|v| Octo(*v,false )).collect();
        self.map.push(octos)
    }

    /* Get the value from the matrix at coordinates x and y if are valid */
    fn get_v(&self, x: i32, y: i32) -> Option<&Octo> {
        let m = &self.map;
        if x < 0 || x >= m.len() as i32 {
            return None;
        }
        if y < 0 || y >= m[0].len() as i32 {
            return None;
        }
        let x = x as usize;
        let y = y as usize;
        return Some(&m[x][y]);
    }

    /* Count the light octopuses */
    fn count_on(&self) -> i32 {
        let mut n: i32 = 0;
        for l in &self.map {
            n += l.iter().filter(|c| c.0 == 9).count() as i32;
        }
        return n;
    }

    /* Do a step and increas all values by one
     * @return - the number of activated octopuses, like count_on() */
    fn step(&mut self) -> i32 {
        let mut n = 0;
        for l in &mut self.map {
            l.iter_mut().for_each(|c| {
                c.1 = false;
                c.0 += 1;
            });
            n += l.iter().filter(|c| c.1 == true).count() as i32;
        }
        return n;
    }

    /* Find the octopuses with energy level greater then 9 and flash
     * move to the surounding ones to increase and flash as well
     * @return - number of total flashes */
    fn spread(&mut self) -> i32 {
        let mut s = 0;
        for i in 0..self.map.len() {
        // for i in 0..3 {
            for j in 0..self.map[i].len() {
                if self.map[i][j].0 > 9 {
                    let res = &mut self.bf(i, j);
                    println!("Spreads ({}, {}): {}",i, j, res);
                    s += *res;
                }
            }
        }
        return s;
    }

    /* Do a BF on the matrix, and spread the energy to other octopuses
     * @return the number of flashes */
    fn bf(&mut self, x: usize, y: usize) -> i32 {
        // queue of the nodes to vizit;
        let mut q: Vec<(i32, i32)> = vec![(x as i32, y as i32)];
        let mut fls = 0; // no of flashes
        while !q.is_empty() {
            let (x, y) = q.pop().unwrap();

            if self.map[x as usize][y as usize].1 == false {
                self.map[x as usize][y as usize].0 += 1;
            }

            if self.map[x as usize][y as usize].0 > 9 &&
                self.map[x as usize][y as usize].1 == false {
                fls += 1;
                self.map[x as usize][y as usize].0 = 0;
                self.map[x as usize][y as usize].1 = true;
            } else {
                continue;
            }

            for (i, j) in [(-1, 0), (1, 0), (0, -1), (0, 1),
                            (-1, -1), (1, 1), (1, -1), (-1, 1)] {
                if let Some(v) = &self.get_v(x + i, y + j) {
                    if v.1 == false {
                        q.push((x + i, y + j));
                    }
                }
            }
        }
        return fls;
    }

    fn print(&self) {
        for l in &self.map {
            l.iter().for_each(|c| print!("{},", c.0));
            println!("");
        }
    }
}

pub fn part1() {
    let content = fs::read_to_string("src/day11/input.txt").expect("No file");
    let splits = content.as_str().trim().split('\n');
    let mut m = Matrix::new();
    for l in splits {
        m.add_line(l);
    }
    let mut flash = 0;
    for _ in 0..100 {
        m.step();
        flash += m.spread();
        m.print();
        println!("Flash: {}", flash);
    }
}

/* When all the octopuses will be syncronized? */
pub fn part2() {
    let content = fs::read_to_string("src/day11/input.txt").expect("No file");
    let splits = content.as_str().trim().split('\n');
    let mut m = Matrix::new();
    for l in splits {
        m.add_line(l);
    }
    let total_oct = (m.map.len() * m.map[0].len()) as i32;
    let mut i = 1;
    loop {
        m.step();
        let flash = m.spread();
        m.print();
        println!("Flash: {}", flash);
        if flash == total_oct {
            println!("Loop number: {}", i);
            break;
        }
        i += 1;
    }
}
